```mermaid
graph LR;
  A-->B;
  A-->C;
  B-->D;
  C-->B;
  C-->D;
  D-->C;
```


2020 I/O MAP

DRIVE - 4EA VEXPRO VICTOR SPX

      LEFT:
            CAN 10
            CAN 11

      RIGHT:
            CAN 13
            CAN 14

BALL HANDLER - 2EA SPARK ON Y CABLE

      PWM 0

      3 LIMIT SWITCHES

            START: DIO 0
            LOAD/EJECT: DIO 1
            SCOOP: DIO 2


BALL EJECTOR

      PWM 1

LIFT - 2EA SPARK MAX

      CAN 20
      CAN 21

      3 LIMIT SWITCHES:

            BOTTOM: DIO 3
            COLOR WHEEL: DIO 4
            TOP: DIO 5

GYRO:
  ANALOG 0

ACCELEROMETER:
  ANALOG 1

SONAR:
  ANALOG 2


COLOR WHEEL - 1EA TALON SRX

      CAN 12




2020 Motors

Quantity     Motor               Controller


4 ea         CIM DRIVE MOTORS    VICTOR SPX

2 ea         NEO LIFT MOTORS     SPARK MAX

2 ea         SNOWBLOWER MOTORS   SPARKS
1/4 FORWARDS DROPS, POWERCELL INTAKE

1 ea         BALL INTAKE/EJECTOR SPARK
FULL REVERSE PICKS UP
FULL FWD SCORES

1 ea          775 PRO COLOR WHEEL TURNING TALON SRX

1 ea          TRAVERSING MOTOR ?



PHYSICAL LIMIT SWITCHES:

3 EZ          POWER CELL      INTAKE / EJECTOR    #1 = MATCH START
                                                  #2 = INTAKE/EJECT
                                                  #3 = SCOOPER

ON PDB:

  40A BREAKERS: 4 DRIVE MOTORS (VICTOR SPX)
                2 NEO LIFT MOTORS (SPARK MAX)
                1 WHEEL TURNING MOTOR (TALON SRX)


  30A BREAKERS:
  RULE 58
                2 EA SNOWBLOWER MOTOR (SPARKS)
                1 EA BALL EJECTOR MOTOS (SPARK)



