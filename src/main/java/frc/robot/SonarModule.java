/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.AnalogInput;

/**
 * Add your docs here.
 */
public class SonarModule {
    // constants
    public static final double INCHES_CONVERSION_FACTOR     = 9.8;  // mV per inch
    public static final double FEET_CONVERSION_FACTOR       = 12;   // inches per foot

    // Member variables
    AnalogInput sonarAnalogInput;

    public SonarModule (int analogPortNumber)
    {
        sonarAnalogInput = new AnalogInput(analogPortNumber);
    }

    public double getRawValue (){
        return sonarAnalogInput.getAverageVoltage();
    }

    public double getInchesValue(){
        double returnValue = (sonarAnalogInput.getAverageVoltage() * 1000) / INCHES_CONVERSION_FACTOR;
        return  returnValue;
    }

    public double getFeetValue(){
        double returnValue = getInchesValue() / FEET_CONVERSION_FACTOR;
        return returnValue;
    }
}
