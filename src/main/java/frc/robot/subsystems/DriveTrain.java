/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotConstants;

public class DriveTrain extends SubsystemBase {
  
  // Members
  private Victor leftMotor;    // Left devboard motor
  private Victor rightMotor;   // right devboard motor

  private VictorSPX leftLeadMotor;
  private VictorSPX leftFollowMotor;
  private VictorSPX rightLeadMotor;
  private VictorSPX rightFollowMotor;

  private MotorType motorType;

  public enum MotorType {Victor, VictorSPX};

  /**
   * Creates a new DriveTrain.
   */
  public DriveTrain(MotorType motorType) {
    this.motorType = motorType;

    switch (motorType){
     
      case Victor:
        leftMotor = new Victor (RobotConstants.LEFT_DEV_DRIVE_PWM);
        rightMotor = new Victor (RobotConstants.RIGHT_DEV_DRIVE_PWM);
        break;

      case VictorSPX:
        leftLeadMotor     = new VictorSPX (RobotConstants.LEFT_LEAD_MOTOR_CAN_ID);
        leftFollowMotor   = new VictorSPX (RobotConstants.LEFT_FOLLOW_MOTOR_CAN_ID);
        rightLeadMotor    = new VictorSPX (RobotConstants.RIGHT_LEAD_MOTOR_CAN_ID);
        rightFollowMotor  = new VictorSPX (RobotConstants.RIGHT_FOLLOW_MOTOR_CAN_ID);

        leftFollowMotor.follow (leftLeadMotor);
        rightFollowMotor.follow (rightLeadMotor);
        break;
    }

  }

  public DriveTrain(){
    this(RobotConstants.DEFAULT_DRIVETRAIN_CONTROLLER_TYPE);
  }

  public void setLeftMotor(double value){
    switch (motorType){
      case Victor:
        leftMotor.set(value);
        break;
      case VictorSPX:
        leftLeadMotor.set(ControlMode.PercentOutput, value);
        break;
    }
  }

  public void setRightMotor(double value){
    switch (motorType){
      case Victor:
        rightMotor.set(-value);
        break;
      case VictorSPX:
        rightLeadMotor.set(ControlMode.PercentOutput, -value);
        break;
    }
  } 
  
  public void setMotorValues(double leftValue, double rightValue){
    setLeftMotor(leftValue);
    setRightMotor(rightValue);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run - about every 50 ms.
  }
}
