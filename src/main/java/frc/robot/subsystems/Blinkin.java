/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.RobotConstants;

public class Blinkin extends SubsystemBase {

  // CONSTANTS
  // Solid Color Constants
  public static final double COLOR_HOT_PINK           = 0.57;
  public static final double COLOR_DARK_RED           = 0.59;
  public static final double COLOR_RED                = 0.61;
  public static final double COLOR_RED_ORANGE         = 0.63;
  public static final double COLOR_ORANGE             = 0.65;
  public static final double COLOR_GOLD               = 0.67;
  public static final double COLOR_YELLOW             = 0.69;
  public static final double COLOR_LAWN_GREEN         = 0.71;
  public static final double COLOR_LIME               = 0.73;
  public static final double COLOR_DARK_GREEN         = 0.75;
  public static final double COLOR_GREEN              = 0.77;
  public static final double COLOR_BLUE_GREEN         = 0.79;
  public static final double COLOR_AQUA               = 0.81;
  public static final double COLOR_SKY_BLUE           = 0.83;
  public static final double COLOR_DARK_BLUE          = 0.85;
  public static final double COLOR_BLUE               = 0.87;
  public static final double COLOR_BLUE_VIOLET        = 0.89;
  public static final double COLOR_VIOLET             = 0.91;
  public static final double COLOR_WHITE              = 0.93;
  public static final double COLOR_GRAY               = 0.95;
  public static final double COLOR_DARK_GRAY          = 0.97;
  public static final double COLOR_BLACK              = 0.99;   // Black is equivalent to "Off"

  // Member variables
  private Spark   blinkinController = new Spark (RobotConstants.BLINKIN_PWM_CHANNEL);
  private double  currentColor;
  private double  primaryColor      = COLOR_WHITE;
  private double  alternateColor    = COLOR_BLACK;
  private int     interval          = 100;
  private int     count             = 0; 
  private boolean blink             = false;
  
  /**
   * Creates a new Blinkin.
   */
  public Blinkin(){
    this(COLOR_WHITE, COLOR_BLACK);
  }

  public Blinkin(double primaryColor){
    this(primaryColor, COLOR_BLACK);
  }
  
  public Blinkin(double primaryColor, double alternateColor){
    this.primaryColor   = primaryColor;
    this.alternateColor = alternateColor;
    currentColor        = primaryColor;
  }

  /**
   * Member functions
   */
  public void setPrimaryColor (double color){
    this.primaryColor = color;
    //blinkinController.set(currentValue);
  }

  public void setAlternateColor(double color){
    this.alternateColor = color;
  }

  public void setInterval(int interval){
    this.interval = interval;
  }

  public void setBlink(boolean blink){
    this.blink = blink;
  }

  @Override
  public void periodic() {
    // This method will be called about every 50 ms...
    if (this.blink == true) {                                       // set to blink...
      if (this.interval > 0 && this.count % this.interval == 0){    // time to Switch colors
        if (this.currentColor == this.primaryColor){                // switch to alternate color
          this.currentColor = this.alternateColor;
        }
        else{                                                       // switch back to primary color
          this.currentColor = this.primaryColor;
        } 
      }
      count++;                                                      // increment loop count 
    }
    else {                                                          // Not blinking... set to primary color...
      this.currentColor = this.primaryColor;            
    }

    this.blinkinController.set(this.currentColor);                  // Set the color on the contorller
  }
}
