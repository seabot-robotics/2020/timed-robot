/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import java.util.Map;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.InstantCommand;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import frc.robot.commands.ArcadeDrive;
import frc.robot.commands.StartBlinkin;
import frc.robot.subsystems.Blinkin;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.DriveTrain.MotorType;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private String m_autoSelected;
  private final SendableChooser<String> m_chooser = new SendableChooser<>();

  DriveTrain.MotorType driveTrainMotorType = MotorType.Victor;
  
  // Joysticks
  private Joystick driverJoystick = new Joystick(RobotConstants.DRIVER_JOYSTICK_PORT);
// private Joystick operatorJoystick = new Joystick(RobotConstants.OPERATOR_JOYSTICK_PORT);

  // Joystick Buttons
  private Button ledToggleButton = new JoystickButton(driverJoystick, 10);
  //                   .whenPressed(()->blinkin.setInterval(10));
  
  // Subsystems
  Blinkin blinkin = new Blinkin();
  DriveTrain driveTrain = new DriveTrain(driveTrainMotorType);

  // Commands
  private Command blinkinCommand
  // Insert 2021 stuff here
  StartBlinkin blinkinCommand = new StartBlinkin(blinkin);
  private final ArcadeDrive  arcadeDrive    = new ArcadeDrive(driveTrain, driverJoystick);

  // Modules
  SonarModule sonar       = new SonarModule(RobotConstants.SONAR_ANALOG_CHANNEL);
  ColorSensor colorSensor = new ColorSensor(I2C.Port.kOnboard);

  enum LEDModes {SONAR, COLOR_SENSOR}

  private LEDModes lightMode = LEDModes.SONAR;


  
  /**
   * This function is run when the robot is first started up and should be
   * used for any initialization code.
   */
  @Override
  public void robotInit() {
    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    SmartDashboard.putData("Auto choices", m_chooser);

    driveTrain.setDefaultCommand(arcadeDrive);

    ShuffleboardTab driveTab = Shuffleboard.getTab("Drive Train");
    driveTab.add(driveTrain);
    driveTab.add("My Slider", 1).withWidget("Number Slider").withProperties(Map.of("Max", 10, "Min", 0));
  }

  /**
   * This function is called every robot packet, no matter the mode. Use
   * this for items like diagnostics that you want ran during disabled,
   * autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {

    // Start the Scheudler - Since doing it here, not needed other places.
    CommandScheduler.getInstance().run();

    // SmartDashboard items that don't update automatically (i.e. are not "Sendable")
    SmartDashboard.putNumber("Sonar: Inches", sonar.getInchesValue());
    SmartDashboard.putNumber("Sonar: Feet", sonar.getFeetValue());
    SmartDashboard.putNumber("Sonar: Voltage", sonar.getRawValue());

    SmartDashboard.putString("Detected Color", colorSensor.getMatchedColorString());
    SmartDashboard.putNumber("Color Confidence", colorSensor.getColorConfidence());
  }

  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to
   * the switch structure below with additional strings. If using the
   * SendableChooser make sure to add them to the chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    m_autoSelected = m_chooser.getSelected();
    // m_autoSelected = SmartDashboard.getString("Auto Selector", kDefaultAuto);
    System.out.println("Auto selected: " + m_autoSelected);

    blinkin.setPrimaryColor(Blinkin.COLOR_GREEN);
    blinkin.setAlternateColor(Blinkin.COLOR_VIOLET);
    blinkin.setBlink(true);
    blinkin.setInterval(20);
    blinkinCommand.schedule();
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
    switch (m_autoSelected) {
      case kCustomAuto:
        // Put custom auto code here
        break;
      case kDefaultAuto:
      default:
        // Put default auto code here
        break;
    }
  }

  @Override
  public void teleopInit() {
    
    blinkin.setPrimaryColor(Blinkin.COLOR_GREEN);
    blinkin.setAlternateColor(Blinkin.COLOR_BLACK);
    blinkin.setInterval(50);
    blinkin.setBlink(true);
    blinkinCommand.schedule();
    
    // Start Command to drive using Joystick
    arcadeDrive.schedule();
    
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopPeriodic() {
    // TODO: Need to setup joystick buttons to control light modes

    // ledToggleButton.whenReleased(()->{ToggleLEDValue();});

    // if ()){
      int value = lightMode.ordinal();
      value++;
      if (value > LEDModes.values().length - 1) {
        value = 0;
      }
    // }

    switch (lightMode){
      case SONAR:
        // Sonar Mode for blinkin... 
        blinkin.setBlink(true);
        double dist = sonar.getInchesValue();
        if ( dist > RobotConstants.SONAR_GREEN_DIST){
          blinkin.setPrimaryColor(Blinkin.COLOR_GREEN);
        } else if (dist > RobotConstants.SONAR_YELLOW_DIST){
          blinkin.setPrimaryColor(Blinkin.COLOR_YELLOW);
        } else {
          blinkin.setPrimaryColor(Blinkin.COLOR_RED);
        }
        blinkin.setInterval((int) sonar.getInchesValue());
        break;
      case COLOR_SENSOR:    
        //ColorSensor mode for blinkin
        blinkin.setBlink(false);
        blinkin.setPrimaryColor(colorSensor.getMatchedBlinkinColor());
        break;
    }



  }
  public void ToggleLEDValue() {
    // int value = lightMode.ordinal();
    // value++;
    // if (value > LEDModes.values().length - 1) {
    //   value = 0;
    // }
    // return new ArcadeDrive(driveTrain, driverJoystick);
  }
  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
  }

  public void ConfigureButtons(){

  }

}
