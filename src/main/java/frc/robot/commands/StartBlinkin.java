/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Blinkin;

public class StartBlinkin extends CommandBase {
  private Blinkin blinkinSubsystem;
  
  /**
   * Creates a new StartBlinkin.
   */

  public StartBlinkin(Blinkin subsystem){
    this(subsystem, Blinkin.COLOR_WHITE);
  }

  public StartBlinkin(Blinkin subsystem, double blinkinColor) {
    // Use addRequirements() here to declare subsystem dependencies.
    this.blinkinSubsystem = subsystem;
    addRequirements(this.blinkinSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    System.out.println("StartBlinkin::Initialize");
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    // Nothing to execute
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    System.out.println("StartBlinkin::end: Interupted = " + interrupted);
  }

  // Returns true when the command should end - This is called periodically.
  @Override
  public boolean isFinished() {
    // Command never finishes... 
    return false;
  }
}
