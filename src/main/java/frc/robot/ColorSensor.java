/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import com.revrobotics.ColorMatch;
import com.revrobotics.ColorMatchResult;
import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.util.Color;
import frc.robot.subsystems.Blinkin;

/**
 * Add your docs here.
 */
public class ColorSensor {
    // Constants
    private static final Color kBlueTarget = ColorMatch.makeColor(0.143, 0.427, 0.429);
    private static final Color kGreenTarget = ColorMatch.makeColor(0.197, 0.561, 0.240);
    private static final Color kRedTarget = ColorMatch.makeColor(0.561, 0.232, 0.114);
    private static final Color kYellowTarget = ColorMatch.makeColor(0.361, 0.524, 0.113);

    // Members
    private I2C.Port i2cPort;
    private ColorSensorV3 colorSensor;
    private ColorMatch colorMatcher;
    private ColorMatchResult match;
    private Color detectedColor;
    
    public ColorSensor (I2C.Port i2cPort){
        // set up members... 
        this.i2cPort        = i2cPort;
        this.colorSensor    = new ColorSensorV3 (i2cPort);
        this.colorMatcher   = new ColorMatch();

        // Add colors to the color matcher
        colorMatcher.addColorMatch(kBlueTarget);
        colorMatcher.addColorMatch(kGreenTarget);
        colorMatcher.addColorMatch(kRedTarget);
        colorMatcher.addColorMatch(kYellowTarget);    
    }

    public Color DetectColor(){
        detectedColor = colorSensor.getColor();
        return detectedColor;
    }

    public String getMatchedColorString() {
        String returnResult = "Unknown";
        match = colorMatcher.matchClosestColor(colorSensor.getColor());

        if (match.color == kBlueTarget) {
            returnResult = "Blue";
        } else if (match.color == kRedTarget) {
            returnResult = "Red";
        } else if (match.color == kGreenTarget) {
            returnResult = "Green";
        } else if (match.color == kYellowTarget) {
            returnResult = "Yellow";
        } 
    
        return returnResult;
    }

    public double getMatchedBlinkinColor(){
        double returnValue = Blinkin.COLOR_BLACK;
        match = colorMatcher.matchClosestColor(colorSensor.getColor());

        if (match.color == kBlueTarget) {
            returnValue = Blinkin.COLOR_BLUE;
        } else if (match.color == kRedTarget) {
            returnValue = Blinkin.COLOR_RED;
        } else if (match.color == kGreenTarget) {
            returnValue = Blinkin.COLOR_GREEN;
        } else if (match.color == kYellowTarget) {
            returnValue = Blinkin.COLOR_YELLOW;
        }

        return returnValue;
    }

    public double getColorConfidence(){
        if (match != null){
            return match.confidence;
        } else {
            return 0;
        }
    }
}
